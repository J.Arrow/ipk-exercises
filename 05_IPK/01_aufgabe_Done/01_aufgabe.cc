#include <iostream>
#include <vector>
#include <cmath>

//--globals--
double pi =3.14;

//-----structs-----
struct Point{
    double x;
    double y;
};

struct Polygon{
    std::vector<Point> v;
};




//-----functions-----

Polygon construct (const std::vector<double>& x, const std::vector<double>& y){

    Polygon polygon;
    if(x.size() != y.size()){
        //return error
    }
    for (int i=0 ; i<x.size() ; i++){
        
        Point p = {x[i],y[i]};
        polygon.v.push_back(p);
    }
    
    for(int i=0 ; i<=polygon.v.size() ; i++){
        std::cout<<"Punkt"<<i<<"= "<<polygon.v[i].x<<","<<polygon.v[i].y<<std::endl;
    }
    
    
    return polygon;
}


double volume (const Polygon& polygon){
    double A = 0.;
    int i = 0;
    
    if(polygon.v.size()<3){
        A = 0;
    }else{
        for(i=0 ; i<polygon.v.size()-1; i++){
            A += polygon.v[i].x * polygon.v[i+1].y - polygon.v[i+1].x * polygon.v[i].y;
        }
        //Anschließend letzten und ersten Punkt betrachten
        A += polygon.v[i].x * polygon.v[0].y - polygon.v[0].x * polygon.v[i].y;
        A = A/2;
    }
    return A;

}




//-----main-----
int main(int argc, char *argv[])
{
  
    int maxN = 8;
    
    std::vector<double> xVector;
    std::vector<double> yVector;
    
    for(int n=1 ; n<=maxN ; n++){
        std::vector<double> xVector;
        std::vector<double> yVector;
        for(int i=1; i<=n ; i++ ){
            xVector.push_back (std::cos(((double)i/(double)n)*2*pi));
            yVector.push_back (std::sin(((double)i/(double)n)*2*pi));
        }
        Polygon polygon = construct(xVector, yVector);
        std::cout<<"volume= "<<volume(polygon)<<std::endl;
        std::cout<<std::endl;
        xVector.clear();
        yVector.clear();
    }
    
    
    
  return 0;
}
