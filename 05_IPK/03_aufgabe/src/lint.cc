#include <iostream>
#include <string>

#include "lint.hh"

bool check_parentheses(std::string symbols){
    
    int openedParantheses = 0;
    
    for (int i=0; i<symbols.length(); ++i)
    {
        if (strncmp(&symbols[i], "(", 1) == 0 ){
            openedParantheses += 1;
        }
        if(strncmp(&symbols[i], ")", 1) == 0){
            openedParantheses -= 1;
        }
        //überprüfe (nach jedem Buchstaben), ob mehr Klammern geschlossen wurden als geöffnet. Falls ja gebe false aus!
        if(openedParantheses < 0){
            return false;
        }
    }
    
    //Sollte es keine offenen Klammern geben --> valid!
    if(openedParantheses == 0){
        return true;
    }
    //Sollte es mehr leftParantheses geben als rightParantheses, so ist mind. eine Klammer noch offen --> inValid!
    else{
        return false;
    }
}
