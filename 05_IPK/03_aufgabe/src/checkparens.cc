#include <iostream>

#include "input.hh"
#include "lint.hh"

int main(int argc, char *argv[])
{
    std::string symbols;
    bool isValid;
    std::cout<<"Geben Sie ihre Zeichenfolge ein und beenden sie die Eingabe durch CTRL + D"<<std::endl;
    
    symbols = read_stream(std::cin);
    
    isValid = check_parentheses(symbols);

    if(isValid == true){
        std::cout<<"symbols are valid"<<std::endl;
    }else{
        std::cout<<"symbols are not valid"<<std::endl;
    }
  return 0;
}
